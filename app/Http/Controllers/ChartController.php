<?php

namespace App\Http\Controllers;

use App\Services\ApiService;
use App\Models\CompanyProfile;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }
    //
    /**
     * this function retreives company info using session data for chart.js plugin
     * */
    public function getChartInfo(Request $request)
    {
        if (session()->has('companyData')) {
            $companyData = $request->session()->get('companyData');

            /**
            * api call for historical data
            * historical data is retreived after filtering data based on startDate and endDate
            * */
            $historicalData = $this->apiService->getHistoricalData($companyData);

            //dd($apiResponseData);
            if (!isset($historicalData) || empty($historicalData)) {
                return redirect('/')->with([
                    'error' => 'Something went wrong while retreiving Historical Data for company : `' . $companyData['symbol'] . "`",
                ]);
            } else {
                $openPrice = array_column($historicalData, 'open');
                $closePice = array_column($historicalData, 'close');
                $date = array_column($historicalData, 'date');

                $companyName = $companyData['companyInfoWithSymbol']->{"Company Name"};
                $companyNameAndSymbol = $companyName."  (".$companyData['symbol'].")";

                $resData = array('openPrice' => $openPrice, 'closePice' => $closePice, 'date' => $date, 'companyNameAndSymbol' => $companyNameAndSymbol);
                return view('chart', $resData);
            }
        } else {
            return redirect('/')->with([
                'error' => "Something went wrong.Please insert company data to procees further"
            ]);
        }
    }
}
