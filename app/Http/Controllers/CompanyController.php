<?php

namespace App\Http\Controllers;

use App\Services\ApiService;
use App\Models\CompanyProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }
    //
    /**
     * this function retreives company info using data sent in post request
     * 
     * */
    public function getCompanyInfo(Request $request)
    {
        //dd($request->session());
        $companyProfile = new CompanyProfile();
        $validator = Validator::make($request->all(), $companyProfile->companyProfileValidationRules);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            //validate company symbol
            $validatedCompanyData = $this->apiService->validateSymbol($request);
            
            
            if ($validatedCompanyData !== false) {
                //dd($companyData);
                $historicalData = $this->apiService->getHistoricalData($validatedCompanyData);
    
                //dd($apiResponseData);
                if (!isset($historicalData) || empty($historicalData)) {
                    return redirect('/')->with([
                        'error' => 'Something went wrong while retreiving Historical Data for company : `' . $validatedCompanyData['symbol'] . "`",
                    ]);
                } else {
    
                    $request->session()->forget('companyData');
    
                    $request->session()->put('companyData', $validatedCompanyData);

                    //dd($companyData);
                    $companyName = $validatedCompanyData['companyInfoWithSymbol']->{"Company Name"};
                    $companyNameAndSymbol = $companyName."  (".$validatedCompanyData['symbol'].")";
                    
                    $resData = array('historicalData' => $historicalData, 'companyNameAndSymbol' => $companyNameAndSymbol);
                    return view('list',$resData);
                }
            } else {
                return redirect('/')->with([
                    'error' => 'Company Symbol Validation Failure',
                ]);
            }
            
        }
    }
    
    /**
     * this function retreives company info using session data.Used in get rquest
     * 
     * */
    public function getCompanyData(Request $request)
    {
        
        if (session()->has('companyData')) {
            $companyData = $request->session()->get('companyData');
            /**
            * api call for historical data
            * historical data is retreived after filtering data based on startDate and endDate
            * */
            $historicalData = $this->apiService->getHistoricalData($companyData);

            if (!isset($historicalData) || empty($historicalData)) {
                
                return redirect('/')->with([
                    'error' => 'Something went wrong while retreiving Historical Data for company : `' . $companyData['companyInfoWithSymbol']['"Company Name"'] . "`",
                ]);
            } else {
                //dd($companyData['companyInfoWithSymbol']->{"Company Name"});
                $companyName = $companyData['companyInfoWithSymbol']->{"Company Name"};
                $companyNameAndSymbol = $companyName."  (".$companyData['symbol'].")";
                $resData = array('historicalData' => $historicalData, 'companyNameAndSymbol' => $companyNameAndSymbol);
                return view('list', $resData);
            }
        } else {
            return redirect('/')->with([
                'error' => "Something went wrong.Please insert company data to procees further"
            ]);
        }
    }
}
