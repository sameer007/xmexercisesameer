<?php

namespace App\Http\Controllers;

use App\Models\CompanyProfile;
use App\Services\EmailService;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    private $emailService;
    
    
    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }
    
    /**
     * this function sends email to email address provided in post request after data retreival
     * 
     * */
    public function sendEmail(Request $request)
    {
        $companyProfile = new CompanyProfile();
        if (session()->has('companyData')) {
            $companyData = $request->session()->get('companyData');

            
            /**
            * api call for historical data
            * historical data is retreived after filtering data based on startDate and endDate
            * */
            $historicalData = $companyProfile->getHistoricalData($companyData);
            $companyName = $companyData['companyInfoWithSymbol']->{"Company Name"};
            $companyNameAndSymbol = $companyName."  (".$companyData['symbol'].")";
            
            if (!isset($historicalData) || empty($historicalData)) {
                return redirect('/')->with([
                    'error' => 'Something went wrong while retreiving Historical Data for company : `' . $companyNameAndSymbol . "`",
                ]);
            } else {
                /**
                * Main code to send email
                * blade template `emailTemplate is used to send data`
                * mailtrap is used to send this email
                * new mailtrap username and password might be required after certain time duration
                * */
                $response = $this->emailService->sendEmail($companyData);

                if ($response['type'] === 'error') {
                    return redirect('/viewChart')->with(['error' => $response['message']]);
                } else {
                    return redirect('/viewChart')->with(['success' => $response['message']]);
                }
                
            }
        } else {
            return redirect('/viewChart')->with([
                'error' => 'Something went wrong while sending email.'
            ]);
        }
    }
}
