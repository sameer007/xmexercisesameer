<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class EmailService
{
    public function sendEmail($companyData)
    {
        
        $companyNameAndSymbol = $companyData['companyInfoWithSymbol']->{"Company Name"}."  (".$companyData['symbol'].")";
        $subject = 'Company share Data retrieved for : `' . $companyNameAndSymbol . "`";

        $mailData = [
            'startDate' => $companyData['startDate'],
            'endDate' => $companyData['endDate'],
        ];
        $user = [
            'to' => $companyData['email'],
            'subject' => $subject,
        ];
        
        Mail::send('layouts/emailTemplate', $mailData, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject($user['subject']);
        });

        if (count(Mail::failures()) > 0) {

            $errors = "Failed to send email of historical data retreival of company `" . $companyNameAndSymbol . "`";
            return [
                'type' => 'error',
                'message' => $errors,
            ];
            
        } else {

            return [
                'type' => 'success',
                'message' => "Mail sent successfully to company `" . $companyNameAndSymbol . "`",
            ];
        }
    }
}