<?php

namespace App\Models;

use ApiService;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

/**
 * Modal to retreive company data
 * 
 * */
class CompanyProfile extends Model
{
    use HasFactory;
    
    public function __construct()
    {
    }
    /**
     * form validation for post request
     * 
     * */
    public $companyProfileValidationRules = [
        'symbol' => 'string|required',
        'startDate' => 'date|before_or_equal:endDate|required',
        'endDate' => 'date|after_or_equal:startDate|required',
        'email' => 'email|required',
    ];
}
