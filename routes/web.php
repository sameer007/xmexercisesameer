<?php

use App\Http\Controllers\ChartController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    
    return view('index');
});
Route::post('getCompanyInfo', [CompanyController::class,'getCompanyInfo'])->name('submitCompanyInfo.post');
Route::get('getCompanyInfo', [CompanyController::class,'getCompanyData']);

Route::get('viewChart', [ChartController::class,'getChartInfo']);
Route::get('sendEmail', [EmailController::class,'sendEmail']);