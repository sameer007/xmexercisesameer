<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic feature test of form submission page.
     * frontend testing
     * @return void
     */
    public function test_getFormForSubmission()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test of post request using form for retreival for Historical Data.
     * backend testing
     * @return void
     */
    public function test_sendCompanyDataPostRequestToRetreiveHistoricalData()
    {
        $response = $this->post('/getCompanyInfo', [
            'symbol' => 'AMRN',
            'startDate' => "1970-01-18",
            'endDate' => "1970-01-21",
            'email' => 'sameer@yahoo.com',
        ]);
        $response->assertSessionHas('companyData');
        $response->assertStatus(200);
    }

    /**
     * A basic feature test to retreive historical data and and show it as chart.
     * backend & frontend testing
     * @return void
     */
    public function test_displayChartShowingHistoricalData()
    {
        
        $response = $this->get('/viewChart');
        
        $response->assertStatus(302);
    }

    /**
     * A basic feature test to retreive Historical data usimg session.
     * backend & frontend testing
     * @return void
     */
    public function test_getHistoricalDataFromGetRequestUsingSession()
    {
        
        $response = $this->get('/getCompanyInfo');
        
        $response->assertStatus(302);
    }
}
