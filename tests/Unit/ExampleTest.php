<?php

namespace Tests\Unit;

use App\Models\CompanyProfile;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    /**
     * Api test for historic data.
     *
     * @return void
     */
    public function test_HistoricDataApiTest()
    {
        $companyProfile = new CompanyProfile();

        $companyData['symbol'] = 'AMRN';
        $companyData['startDate'] = '1970-01-18';
        $companyData['endDate'] = '1970-01-21';
        $companyData['email'] = 'sameer@yahoo.com';

        $historicData = $companyProfile->getHistoricalData($companyData);
        $this->assertTrue(isset($historicData));
        
    }
}
