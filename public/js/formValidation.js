$(document).ready(function() {
    // Get the current date in YYYY-MM-DD format
    var currentDate = new Date().toISOString().split('T')[0];
    var endDate = $('#endDate');
    var startDate = $('#startDate');
    
    // Set the 'max' attribute of the "End Date" input to the current date
    startDate.attr('max', currentDate);
    endDate.attr('max', currentDate);
    
    startDate.on('change', function() {
        var stDate = $(this).val();

        endDate.attr('min', stDate);
    });

    endDate.on('change', function() {
        var enDate = $(this).val();
        
        startDate.attr('max', enDate);
    });
});