@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->

    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> Insert Company details for Historical Data</h4>
                        <form id="companyDetailsForm" class="form-horizontal style-form" action="./getCompanyInfo" METHOD="POST">
                            @csrf()
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Company Symbol</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="symbol" id="symbol">

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label"> Start Date</label>
                                <div class="col-sm-3">
                                    <input type="date" name="startDate" id="startDate" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">End Date</label>
                                <div class="col-sm-3">
                                    <input type="date" name="endDate" id="endDate" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Email</label>
                                <div class="col-sm-3">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="User Email" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit">Submit</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script type="text/javascript" src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>

    @section('customJs')
    <script type="text/javascript" src="{{url('lib/jquery.backstretch.min.js')}}"></script>
    <script>
        $.backstretch("{{url('img/instagram.jpg')}}", {
            speed: 500
        });
    </script>
    <!--script for this page-->
    <!--retreiving company symbols using api call-->
    <script type="text/javascript" src="{{url('js/companyApiCall.js')}}"></script>

    <script type="text/javascript" src="{{url('js/formValidation.js')}}"></script>
    
    @endsection
</section>
@endsection