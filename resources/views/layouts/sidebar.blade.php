<aside>
    @php

    $currentPage = substr(strrchr(url()->current(),"/"),1);
    $profileUrl = url('https://cloud.xm-cdn.com/static/xm/common/logos/XMLogo-2021_homepage.svg');


    @endphp

    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="profile.html"><img src="{{$profileUrl}}" class="img-square" width="80"></a></p>
            <h5 class="centered">User</h5>
            <li class="mt">
                <a class="{{ ($currentPage == 'getCompanyInfo') ? 'active' : ''  }}" href="{{url('getCompanyInfo')}}">
                    <i class="fa fa-table"></i>
                    <span>View Company Info</span>
                </a>
            </li>
            <li class="mt">
                <a class="{{ ($currentPage == 'viewChart') ? 'active' : ''  }}" href="{{url('viewChart')}}">
                    <i class="fa fa-data"></i>
                    <span>View Chart</span>
                </a>
            </li>
            <li class="mt">
                <a class="{{ ($currentPage == '/') ? 'active' : ''  }}" href="{{url('/')}}">
                    <i class="fa fa-data"></i>
                    <span>Check New Company Info</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>