@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->

    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                
                <div class="col-lg-12">
                    @include('layouts.notify')
                    
                    <h4><i class="fa fa-angle-right"></i> Historical Data Chart of company {{(isset($companyNameAndSymbol) && !empty($companyNameAndSymbol)) ? "`".$companyNameAndSymbol."`" : '' }} <a class="btn btn-primary pull-right" href="{{url('/sendEmail')}}" onclick="return confirm('Do you want to send an email about this data to company `{{$companyNameAndSymbol}}`?');">Send Email </a> </h4>
                    <canvas id="myChart"></canvas>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script type="text/javascript" src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>

    @section('customJs')

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-date-fns/dist/chartjs-adapter-date-fns.bundle.min.js"></script>

    <script type="text/javascript">
        const ctx = document.getElementById('myChart');

        let date = JSON.parse('{{ json_encode($date) }}');
        let openPrice = JSON.parse('{{ json_encode($openPrice) }}');
        let closePice = JSON.parse('{{ json_encode($closePice) }}');

        const data = {
            labels: date,
            datasets: [{
                    label: 'Open Price',
                    data: openPrice,
                    backgroundColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1,
                    fill: false
                },
                {
                    label: 'Close Price',
                    data: closePice,
                    backgroundColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    fill: false
                }
            ]
        };
        new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                scales: {
                    x: {
                        type: 'time', // Use time scale for X-axis (dates)
                        
                        title: {
                            display: true,
                            text: 'Datetime',
                        },
                    },
                    y: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Price ($)'
                        }
                    }
                }
            }
        });
    </script>
    @endsection 
    
</section>
@endsection