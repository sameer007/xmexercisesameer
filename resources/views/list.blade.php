@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->

    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
                <div class="col-lg-12">
                    <table class="table table-striped table-advance table-hover">
                        @php
                            

                            
                        @endphp
                        <h4><i class="fa fa-angle-right"></i> Historical Data List of company {{(isset($companyNameAndSymbol) && !empty($companyNameAndSymbol)) ? "`".$companyNameAndSymbol."`" : '' }} <a class="btn btn-primary pull-right" href="{{url('/viewChart')}}">View Chart </a> </h4>
                        <hr />
                        
                        <thead>
                            <tr>
                                <th><i class="fa fa-datetime"></i>Date</th>
                                <th><i class="fa fa-dollar"></i> Open</th>
                                <th><i class="fa fa-dollar"></i> High</th>
                                <th><i class="fa fa-dollar"></i> Low</th>
                                <th><i class="fa fa-dollar"></i> Close</th>
                                <th><i class="fa fa-unit"></i> Volume (Units)</th>
                            </tr>
                        </thead>
                        <tbody id="bookListTbody">
                        
                            @if (isset($historicalData) && !empty($historicalData))
                                
                                    
                                    @foreach ($historicalData as $items)
                                    @php
                                        $carbon = \Carbon\Carbon::createFromTimestampMs($items->date);
                                        $dateTime = $carbon->format('Y-M-d h:i:s A');
                                    @endphp
                                    <tr>
                                        <td>{{$dateTime}}</td>
                                        <td>${{$items->open}}</td>
                                        <td>${{$items->high}}</td>
                                        <td>${{$items->low}}</td>
                                        <td>${{$items->close}}</td>
                                        <td>{{$items->volume}}</td>
                                    </tr>  
                                    @endforeach
                                    <br/>
                            @else
                            <tr>
                                <td>No data</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script type="text/javascript" src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>

    @section('customJs')
    <script type="text/javascript" src="{{url('lib/jquery.backstretch.min.js')}}"></script>

    <!-- <script type="text/javascript" src="{{url('js/companyApiCall.js')}}"></script>

    <script type="text/javascript" src="{{url('js/formValidation.js')}}"></script> -->

    <!-- <script>
        $.backstretch("{{url('img/instagram.jpg')}}", {
            speed: 500
        });
    </script> -->

    <!--script for this page-->
    <!-- <script type="text/javascript" src="{{url('lib/form-validation-script.js')}}"></script> -->
    @endsection 
</section>
@endsection